cd testing
test_transfers;
test_catch;
test_fortran1;
test_fortran2;
cd ../example

cd foobar;    foobar;         cd ..
cd zlib;      testgz;         cd ..

cd eventq; 
testq_plain;
testq_class;
cd ..

cd eventq2;
testq2;
cd ..

cd fem;
test_assembler; 
test_simple;
test_patch;
cd ..
