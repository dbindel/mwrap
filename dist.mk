all: dist

# === Version information ===

VERS=0.33
MWRAP=mwrap-$(VERS)

include make.inc
include Makefile

# === Tag targets ===

tag:
	svn copy -m "Tag $(VERS)." \
		file:///scratch/svn/wrap/trunk \
		file:///scratch/svn/wrap/tags/$(VERS)

# === Tar / distribution targets ===

SRC=\
	README COPYING NEWS Makefile make.inc mwrap.pdf doc/mwrap.tex \
	src/Makefile src/mwrap.y src/mwrap.l \
	src/*.hh src/*.h src/*.c src/*.cc \
	testing/Makefile testing/test_all.m testing/*.mw testing/*.ref \
	example/Makefile \
	example/eventq/Makefile example/eventq/README \
	example/eventq/eventq_class.mw example/eventq/eventq_plain.mw \
	example/eventq/eventq_handle.mw \
	example/eventq/testq_class.m example/eventq/testq_plain.m \
	example/eventq/testq_handle.m \
	example/eventq2/Makefile example/eventq2/README \
	example/eventq2/eventq2.mw example/eventq2/testq2.m \
	example/zlib/Makefile example/zlib/README \
	example/zlib/gzfile.mw example/zlib/testgz.m \
	example/foobar/Makefile example/foobar/foobar.mw \
	example/eventq/testq.m \
	example/fem/Makefile example/fem/README \
	example/fem/init.m example/fem/test*.m \
	example/fem/interface/Makefile example/fem/interface/*.mw \
	example/fem/src/Makefile example/fem/src/*.cc example/fem/src/*.h

README: README.in dist.mk
	echo $(MWRAP) > tmp.txt
	cat tmp.txt README.in > README
	rm tmp.txt

doc/index.html: doc/index0.html dist.mk
	cat doc/index0.html | \
	sed s:mwrap\\.tar\\.gz:$(MWRAP).tar.gz: >doc/index.html

mwrap.pdf: doc/mwrap.tex
	(cd doc; make mwrap.pdf; cp mwrap.pdf ..)

dist: tgz
	tar -xzf ../$(MWRAP).tar.gz
	cp dist-make.inc $(MWRAP)/make.inc
	cp mtest.m $(MWRAP)/mtest.m
	(cd $(MWRAP); make bin test demo; octave mtest.m)
	rm -rf $(MWRAP)

tgz: $(MWRAP).tar.gz

$(MWRAP).tar.gz: clean src/mwrap.cc src/lex.yy.c mwrap.pdf README
	ls $(SRC) | sed s:^:$(MWRAP)/: >MANIFEST
	(ln -s `pwd` ../$(MWRAP))
	(cd ..; tar -czvf $(MWRAP).tar.gz `cat $(MWRAP)/MANIFEST`)
	(cd ..; rm $(MWRAP))

upload: dist doc/index.html mwrap.pdf
	scp ../$(MWRAP).tar.gz doc/index.html mwrap.pdf \
		access:public_html/mwrap
	ssh access chmod a+r \
		public_html/mwrap/$(MWRAP).tar.gz \
		public_html/mwrap/index.html \
		public_html/mwrap/mwrap.pdf

web-upload: doc/index.html 
	scp doc/index.html access:public_html/mwrap
	ssh access chmod a+r public_html/mwrap/index.html 
