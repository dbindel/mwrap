% Run all demos.

cd foobar;    foobar;         cd ..
cd zlib;      testgz;         cd ..

cd eventq; 
testq_plain;
testq_class;
cd ..

cd eventq2;
testq2;
cd ..

cd fem;
test_assembler; 
test_simple;
test_patch;
cd ..
